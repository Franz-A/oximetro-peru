declare module '@ant-design-vue/pro-layout' {
  import Vue, {VNode} from "vue";
  import {RouteConfig} from "vue-router";
  type VueClass<V> = {
    new(...args: any[]): V & Vue;
  } & typeof Vue;

  function Mixins<A>(CtorA: VueClass<A>): VueClass<A>;
  function Mixins<A, B>(
    CtorA: VueClass<A>,
    CtorB: VueClass<B>
  ): VueClass<A & B>;
  function Mixins<A, B, C>(
    CtorA: VueClass<A>,
    CtorB: VueClass<B>,
    CtorC: VueClass<C>
  ): VueClass<A & B & C>;
  function Mixins<A, B, C, D>(
    CtorA: VueClass<A>,
    CtorB: VueClass<B>,
    CtorC: VueClass<C>,
    CtorD: VueClass<D>
  ): VueClass<A & B & C & D>;
  function Mixins<A, B, C, D, E>(
    CtorA: VueClass<A>,
    CtorB: VueClass<B>,
    CtorC: VueClass<C>,
    CtorD: VueClass<D>,
    CtorE: VueClass<E>
  ): VueClass<A & B & C & D & E>;
  function Mixins<T>(...Ctors: VueClass<Vue>[]): VueClass<T>;

  export class SettingDrawer extends Vue {
    //props
    theme: 'light' | 'dark';
    layout: 'sidemenu' | 'topmenu';
    primaryColor: string;
    contentWidth: 'Fixed' | 'Fluid';
  }
  export class PageHeaderWrapper extends Vue {
    content: VNode | vSlot;
    extra: VNode | vSlot;
    extraContent: VNode | vSlot;
    tabList: array<{key: string, tab: sting}>;
    tabChange:  	(key) => void;
    tabActiveKey: string
  }

  export default class extends Mixins(SettingDrawer) {
    // props
    /**
     * @default null
     */
    title: VNode | string 	;
    /**
     * @default []
     */
    menus: Array<RouteConfig>;
    logo: VNode | render;
    loading: boolean;
    collapsed: boolean;
    isMobile: boolean;
    autoHideHeader: boolean;
    mediaQuery: array<any>;
    handleCollapse: (collapsed: boolean) => void;
    handleMediaQuery: (querys: []) => void;
    menuHeaderRender: vSlot | VNode  | ((logo: VNode | render,title: VNode | String )=>(VNode))| false;
    headerRender: (props: BasicLayoutProps) => VNode;
    rightContentRender: (props: HeaderViewProps) => VNode;
    collapsedButtonRender: (collapsed: boolean) => VNode;
    footerRender: (props: BasicLayoutProps) => VNode;
    breadcrumbRender: ({ route, params, routes, paths, h }) => VNode[];
    i18nRender:  ((key: string) => string) | false;

  }

  export function updateColorWeak(colorWeak: boolean): any;

  export function updateTheme(primaryColor: string): any;
}


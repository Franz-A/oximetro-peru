export default abstract class Storage {
  static set(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value))
  }
  static get(key: string, defaultValue: any): any|null {
    const item= localStorage.getItem(key)
    if(item)
      return JSON.parse(item)
    else {
      this.set(key,defaultValue)
      return defaultValue
    }
  }
}

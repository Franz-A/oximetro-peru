import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import '@/plugins/ant-design-vue'
import themeConfig from './config/theme.config'
import Initializer from "@/core/bootstrap";
import VueGtag from "vue-gtag";
Vue.config.productionTip = false
/* eslint-disable @typescript-eslint/camelcase */
window.umi_plugin_ant_themeVar = themeConfig.theme;


Vue.use(VueGtag, {
  config: { id: "G-FX8YNFPQFW" }
}, router);

new Vue({
  router,
  store,
  created: Initializer,
  render: (h) => h(App)
}).$mount('#app')

import {db} from "@/config/firebase";
import {Department, Province} from "../model";
import {ProvincesState} from "@/store/types/ProvincesState";
import { Commit } from 'vuex';

const provinces = {
  namespaced: true,
  state: () => ({
    provinces: [],
    department: new Department('nF8apPdQhdpkXHzv9rsu', 'Lima')
  } as ProvincesState),
  getters: {
    getDepartment(state: ProvincesState) {
      return state.department
    },
  },
  mutations: {
    setDepartment(state: ProvincesState, department: Department) {
      state.department = department;
    },
    setProvinces(state: ProvincesState, provinces: Array<Province>) {
      state.provinces = provinces;
    },
    addProvince(state: ProvincesState, province: Province) {
      state.provinces.push(province);
    },
  },
  actions: {
    updateDepartment({commit}: { commit: Commit }, department: Department) {
      commit("setDepartment", department)
    },
    syncProvinces: async function (
      {state, commit}: { state: ProvincesState ; commit: Commit },
      options = {
        where: {fieldPath: 'havePoints', opStr: '==', value: true}
      }  as { where: any}) {
      const querySnapshot = await db.collection("departamentos/" + state.department.id + '/provincias').where(
        options.where.fieldPath, options.where.opStr, options.where.value
      ).get();
      for (const doc1 of querySnapshot.docs) {
        const data = doc1.data();
        commit("addProvince", data)
      }
    }
  },
}

export default provinces;

import {db} from "@/config/firebase";
import {DepartmentsState} from "@/store/types/DepartmentsState";
import {Department} from "@/store/model";
import {ActionContext} from "vuex";
import RootState from "@/store/types/RootState";


const departments = {
  namespaced: true,
  state: () => ({
    departments: [],
  }as DepartmentsState),
  mutations: {
    setDepartment(state: DepartmentsState, departments: Array<Department>) {
      state.departments = departments;
    },
    addDepartment(state: DepartmentsState, department: Department) {
      state.departments.push(department);
    },
  },
  actions: {
    syncDepartments: async (
      {commit}: ActionContext<DepartmentsState, RootState>,
      options = { where: {fieldPath: 'havePoints', opStr: '==', value: true} } as { where: any}) => {
      const querySnapshot = await db.collection("departamentos").where(
        options.where.fieldPath, options.where.opStr, options.where.value
      ).get();
      for (const doc1 of querySnapshot.docs) {
        const data = doc1.data();
        commit("addDepartment", data)
      }
    }
  },
  getters: {}
}

export default departments;

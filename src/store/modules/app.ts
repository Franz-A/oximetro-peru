import storage from '@/plugins/storage'
import {
  SIDEBAR_TYPE,
  TOGGLE_COLOR,
  TOGGLE_CONTENT_WIDTH,
  TOGGLE_FIXED_HEADER,
  TOGGLE_FIXED_SIDEBAR,
  TOGGLE_HIDE_HEADER,
  TOGGLE_LAYOUT,
  TOGGLE_MOBILE_TYPE,
  TOGGLE_MULTI_TAB,
  TOGGLE_NAV_THEME,
  TOGGLE_WEAK,
} from '@/store/mutation-types'
import AppState from "@/store/types/AppState";

const app = {
  state: {
    sideCollapsed: false,
    isMobile: false,
    theme: 'dark',
    layout: 'topmenu',
    contentWidth: 'Fixed',
    fixedHeader: false,
    fixedSidebar: false,
    autoHideHeader: false,
    color: 'dark',
    weak: false,
    multiTab: true,
    _antLocale: {}
  } as AppState,
  mutations: {
    [SIDEBAR_TYPE]: (state: AppState, type: boolean): void => {
      state.sideCollapsed = type
      storage.set(SIDEBAR_TYPE, type)
    },
    [TOGGLE_MOBILE_TYPE]: (state: AppState, isMobile: boolean): void => {
      state.isMobile = isMobile
    },
    [TOGGLE_NAV_THEME]: (state: AppState, theme: string): void => {
      state.theme = theme
      storage.set(TOGGLE_NAV_THEME, theme)
    },
    [TOGGLE_LAYOUT]: (state: AppState, mode: string): void => {
      state.layout = mode
      storage.set(TOGGLE_LAYOUT, mode)
    },
    [TOGGLE_FIXED_HEADER]: (state: AppState, mode: boolean): void => {
      state.fixedHeader = mode
      storage.set(TOGGLE_FIXED_HEADER, mode)
    },
    [TOGGLE_FIXED_SIDEBAR]: (state: AppState, mode: boolean): void => {
      state.fixedSidebar = mode
      storage.set(TOGGLE_FIXED_SIDEBAR, mode)
    },
    [TOGGLE_CONTENT_WIDTH]: (state: AppState, type: string): void => {
      state.contentWidth = type
      storage.set(TOGGLE_CONTENT_WIDTH, type)
    },
    [TOGGLE_HIDE_HEADER]: (state: AppState, type: boolean): void => {
      state.autoHideHeader = type
      storage.set(TOGGLE_HIDE_HEADER, type)
    },
    [TOGGLE_COLOR]: (state: AppState, color: string): void => {
      state.color = color
      storage.set(TOGGLE_COLOR, color)
    },
    [TOGGLE_WEAK]: (state: AppState, mode: boolean): void => {
      state.weak = mode
      storage.set(TOGGLE_WEAK, mode)
    },
    [TOGGLE_MULTI_TAB]: (state: AppState, bool: boolean): void => {
      storage.set(TOGGLE_MULTI_TAB, bool)
      state.multiTab = bool
    },
  }
}

export default app

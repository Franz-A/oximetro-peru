import {db} from "@/config/firebase";
import {Feature, FeaturesCollection} from "../model";
import MeasuringPointsState from '../types/MeasuringPointsState'
import {Commit} from "vuex";

const measuringPoints = {
  namespaced: true,
  state: () => ({
    featureCollection: new FeaturesCollection([])
  } as MeasuringPointsState ),
  mutations: {
    setFeatureCollection(state: MeasuringPointsState, featureCollections: FeaturesCollection) {
      state.featureCollection = featureCollections;
    },
    addFeature(state: MeasuringPointsState, feature: Feature) {
      state.featureCollection.features.push(feature);
    },
  },
  actions: {
    syncFeatureCollection: async function ({commit}: { commit: Commit }) {
      const querySnapshot = await db.collection("FeatureCollection").withConverter(Feature.converter).get();
      for (const doc1 of querySnapshot.docs) {
        const data = doc1.data();
        commit("addFeature", data)
      }
    }
  },
  getters: {}
}

export default measuringPoints;

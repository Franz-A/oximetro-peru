import {asyncRoutes, routes} from '@/router'
import {RouteConfig} from "vue-router";
import PermissionState from "@/store/types/PermissionState";
import { Commit } from 'vuex';

function hasPermission (permission: Array<any>, route: RouteConfig): boolean {
  if (route.meta) {
    if (route.meta.permission) {
      let flag = false
      for (let i = 0, len = permission.length; i < len; i++) {
        flag = route.meta.permission.includes(permission[i])
        if (flag) {
          return true
        }
      }
      return false
    }
  }
  return true
}

interface Roles {
  id: string | number;
  permissionList: Array<any>;
}
function hasRole(roles: Roles, route: RouteConfig): boolean {
  if (route.meta && route.meta.roles) {
    return route.meta.roles.includes(roles.id)
  } else {
    return true
  }
}

function filterAsyncRouter (routerMap: Array<RouteConfig>, roles: Roles): Array<RouteConfig> {
  return routerMap.filter((route: RouteConfig) => {
    if (hasPermission(roles.permissionList, route)) {
      if (route.children && route.children.length) {
        route.children = filterAsyncRouter(route.children, roles)
      }
      return true
    }
    return false
  })
}


const permission = {
  state: () => ({
    routers: routes,
    addRouters: [],
  } as PermissionState),
  mutations: {
    SET_ROUTERS: (state: PermissionState, routers: Array<RouteConfig>) => {
      state.addRouters = routers
      state.routers = routes.concat(routers)
    },
  },
  actions: {
    GenerateRoutes: function ({commit: commit}: { commit: Commit } , {roles}: { roles: Roles }) {
      return new Promise(resolve => {
        const accessedRouters = filterAsyncRouter(asyncRoutes, roles)
        commit('SET_ROUTERS', accessedRouters)
        resolve()
      })
    },
  },
}

export default permission

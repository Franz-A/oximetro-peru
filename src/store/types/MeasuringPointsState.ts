import {FeaturesCollection} from "@/store/model";

export default interface MeasuringPointsState {
  featureCollection: FeaturesCollection;
}

import AppState from "@/store/types/AppState";
import PermissionState from "@/store/types/PermissionState";

export default interface RootState {
  app: AppState;
  permission: PermissionState;
}

import {Department, Province} from "@/store/model";

export interface ProvincesState {
  provinces: Array<Province>;
  department:  Department;
}

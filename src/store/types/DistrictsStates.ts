import {District, Province} from "@/store/model";

export interface DistrictsStates {
    districts: Array<District>;
    province: Province;

}

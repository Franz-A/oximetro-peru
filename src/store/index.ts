import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import app from "@/store/modules/app";
import permission from "@/store/modules/permision";
import RootState from "@/store/types/RootState";
import districts from '@/store/modules/districts'
import departments from "@/store/modules/departments";
import provinces from "@/store/modules/provinces";
import measuringPoints from "@/store/modules/measuringPoints";

export default new Vuex.Store({
  getters: {
    isMobile: (state: RootState) => state.app.isMobile,
    theme: (state: RootState) => state.app.theme,
    color: (state: RootState) => state.app.color,
    multiTab: (state: RootState) => state.app.multiTab,
    isTopMenu (state: RootState) {
      return state.app.layout === 'topmenu'
    },
  },
  modules: {
    app,
    permission,
    departments: departments,
    provinces: provinces,
    districts,
    measuringPoints: measuringPoints
  }
})


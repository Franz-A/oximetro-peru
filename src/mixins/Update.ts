import Vue, {CreateElement} from 'vue'
import {notification} from "ant-design-vue";


export  default Vue.extend( {
    data() {
        return {
            // refresh variables
            refreshing: false,
            registration: {} as ServiceWorkerRegistration,
            updateExists: false
        }
    },
    methods: {
        // Store the SW registration so we can send it a message
        // We use `updateExists` to control whatever alert, toast, dialog, etc we want to use
      // To alert the user there is an update they need to refresh for
      updateAvailable(event: any): void {
        this.registration = event.detail
        this.updateExists = true
      },

      // Called when the user accepts the update
      refreshApp(): void {
        this.updateExists = false
        // Make sure we only send a 'skip waiting' message if the SW is waiting
        if (!this.registration || !this.registration.waiting) return
        // send message to SW to skip the waiting and activate the new SW
        this.registration.waiting.postMessage({type: 'SKIP_WAITING'})
      },
      openNotification: function () {
        const key = `open${Date.now()}`;
        notification.open({
          message: 'Nueva Versión Encontrada',
          icon: (h: CreateElement) => {
            return h(
              'a-icon', {
                props: {
                  type: 'reload'

                },
                style: {
                  color: 'primary'
                }
              }, ''
            );
          },
          description: '',
          btn:( h: CreateElement) => {
            return h(
              'div', [
                h(
                  'a-button', {
                    props: {
                      type: 'primary',
                      size: 'large',
                      block: true
                    },
                    on: {
                      click: ()=> this.refreshApp(),
                    },
                  },
                  'Actualizar'
                )
              ]
            );
          },
          key,
          onClose: close,
        });
      },
    } ,
    created: function () {
      // Listen for our custom event from the SW registration
      document.addEventListener('swUpdated', this.updateAvailable, {once: true})
      // Refresh all open app tabs when a new service worker is installed.
      if ('serviceWorker' in navigator) {
        // Prevent multiple refreshes
        navigator.serviceWorker.addEventListener('controllerchange', () => {
          if (this.refreshing) return
          this.refreshing = true
          // Here the actual reload of the page occurs
          window.location.reload()
        })
      }
    },
  watch:{
    updateExists:{
      immediate: true,
      handler(value) {
        if (value)
          this.openNotification()
      }
    }
  }
})

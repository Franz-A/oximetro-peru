import { mapState } from 'vuex'
import RootState from "../store/types/RootState";

const deviceMixin = {
  computed: {
    ...mapState({
      isMobile: function (state: RootState) {
        return state.app.isMobile;
      },
    }),
  },
}

export { deviceMixin }

import Vue from 'vue'
import VueRouter, {RouteConfig} from 'vue-router'
import {RouteConfigMultipleViews, RouteConfigSingleView} from "vue-router/types/router";

Vue.use(VueRouter)

type MyRouteConfig = RouteConfig & {hidden: boolean} | RouteConfig
const asyncRoutes: Array<RouteConfig> = [
]

const routes: Array<MyRouteConfig> = [
  {
    path: '/',
    name: 'Inicio',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    props: route => ({
      point: route.query.point
    }),
    meta: {
      title: "Inicio",
      keepAlive: true,
      icon: "home"
    }
  },
  {
    path: '/team',
    name: 'Equipo',
    component: () => import(/* webpackChunkName: "team" */'@/views/Team.vue'),
    meta: {
      title: "Equipo",
      keepAlive: true,
      icon: "team"
    }
  },
/*  {
    path: '/gift',
    name: 'Ayudanos',
    component: () => import(/!* webpackChunkName: "gift" *!/'../views/Gift.vue'),
    meta: {
      title: "Ayúdanos",
      keepAlive: true,
      icon: "gift"
    }
  },*/
  {
    path: '/contact',
    name: 'Contact',
    component: () => import(/* webpackChunkName: "contactanos" */'../views/Contact.vue'),
    meta: {
      title: "Contacto",
      keepAlive: true,
      icon: "contacts"
    }
  },

/*  {
    path: '/sponsors',
    name: 'Patrocinadores',
    component: () => import(/!* webpackChunkName: "sponsors" *!/'../views/Sponsors.vue'),
    meta: {
      title: "Patrocinadores",
      keepAlive: true,
      icon: "solution"
    }
  },*/
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
    meta: {
      title: "Acerca de Nosotros",
      keepAlive: true,
      icon: "info-circle"
    }
  },
  {
    path: '*',
    name: 'error',
    component:()=> import(/* webpackChunkName: "error" */ "@/views/exception/error.vue"),
    props: () => ({
      code: '404',
    }),
    //
    hidden: true
  }
]
export {routes,asyncRoutes}

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

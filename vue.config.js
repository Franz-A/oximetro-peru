// eslint-disable-next-line @typescript-eslint/no-var-requires
const createThemeColorReplacerPlugin = require('./config/theme.plugin')
// eslint-disable-next-line @typescript-eslint/no-var-requires
const webpack = require('webpack');
module.exports = {
  // ...other vue-cli plugin options...
  pwa: {
    name: 'Oxímetro Perú',
    themeColor: '#00a7ff',
    msTileColor: '#ffffff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: '#ffffff',


  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
        },
      },
    },
  },
  chainWebpack: (config) => {
        config
            .plugin('html')
            .tap((args) => {
                args[0].title = 'Oxímetro Perú'
                return args
            })
    },
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      config.plugins.push(createThemeColorReplacerPlugin);
      console.log("Create theme")
    }
    config.plugins.push(new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/))

  }
}

